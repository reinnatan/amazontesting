
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

public class TestingAmazon {
    private WebDriver driver;

    @BeforeClass
    public void initWebdriver() throws Exception{
       driver = new FirefoxDriver();
        driver.navigate().to("https://www.amazon.com/");
        driver.manage().window().maximize();

    }

    @Test
    public void testSearchingTVLGProductUnder32() throws Exception{
        Thread.sleep(5000);
        driver.findElement(By.linkText("All")).click();
        Thread.sleep(5000);
        driver.findElement(By.linkText("Electronics")).click();
        Thread.sleep(5000);
        driver.findElement(By.linkText("Television & Video")).click();
        Thread.sleep(5000);
        driver.findElement(By.linkText("Televisions")).click();
        Thread.sleep(5000);
        driver.findElement(By.linkText("LG")).click();
        Thread.sleep(5000);
        driver.findElement(By.linkText("32 Inches & Under")).click();
        Thread.sleep(5000);
        Assert.assertEquals(true, driver.getPageSource().contains("LG Electronics"));

    }


    @Test(priority = 1)
    public void testClickOneOfLGElectronicsProduct() throws Exception{
        List<WebElement> elements = driver.findElements(By.className("a-size-base-plus"));
        String text1 = elements.get(0).getText().replace("&nbsp;", "");
        driver.findElement(By.linkText(text1)).click();
        Thread.sleep(5000);
        String textAddList = driver.findElement(By.cssSelector("a[class='a-button-text a-text-left']")).getText();
        Assert.assertEquals("Add to List", textAddList);
    }

    @Test(priority = 2)
    public void testAddListAndLogin() throws Exception{
        driver.findElement(By.linkText("Add to List")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ap_email")).sendKeys("abcd@gmail.com");
        driver.findElement(By.id("continue")).click();
        Thread.sleep(5000);
        driver.findElement(By.id("ap_password")).sendKeys("123");
        driver.findElement(By.id("signInSubmit")).click();
        Thread.sleep(5000);
        String alert = driver.findElement(By.className("a-alert-heading")).getText();
        Assert.assertEquals("Important Message!", alert);
    }


    @AfterClass
    public void finishedExecuteAllTest(){
        System.out.println("FINISHED ALL TEST.....");
        driver.quit();
    }

}
